import {COMMENT_SUCCESS, GET_ONE_NEWS, NEWS_SUCCESS} from "./actions";

const initialState = {
    news: [],
    getOneNews: [],
    comments: [],
};

const cardReducer = (state = initialState, action) => {
    switch (action.type) {
        case NEWS_SUCCESS:
            return {
                ...state,
                news: action.news,
            };
        case GET_ONE_NEWS:
            return {
                ...state,
                getOneNews: action.oneNews
            };
        case COMMENT_SUCCESS:
            return {
                ...state,
                comments: action.comments
            };
        default:
            return state;
    }
};


export default cardReducer;
