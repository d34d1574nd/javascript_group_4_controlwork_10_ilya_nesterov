import axios from '../axiosNews';

export const NEWS_REQUEST = 'NEWS_REQUEST';
export const NEWS_SUCCESS = 'NEWS_SUCCESS';
export const NEWS_FAILURE = 'NEWS_FAILURE';
export const GET_ONE_NEWS = 'GET_ONE_NEWS';
export const COMMENT_REQUEST = 'COMMENT_REQUEST';
export const COMMENT_SUCCESS = 'COMMENT_SUCCESS';
export const COMMENT_FAILURE = 'COMMENT_FAILURE';

export const newsRequest = () => ({type: NEWS_REQUEST});

export const newsSuccess = news => ({type: NEWS_SUCCESS, news});

export const newsFailure = error => ({type: NEWS_FAILURE, error});

export const getOneNews = oneNews => ({type: GET_ONE_NEWS, oneNews});

export const commentRequest = () => ({type: COMMENT_REQUEST});

export const commentSuccess = comments => ({type: COMMENT_SUCCESS, comments});

export const commentFailure = error => ({type: COMMENT_FAILURE, error});

export const gettingNews = () => {
    return (dispatch) => {
        dispatch(newsRequest());
        axios.get('/news').then(response => {
            dispatch(newsSuccess(response.data));
        }, error => {
            dispatch(newsFailure(error));
        });
    }
};

export const addNewNews = news => {
    return (dispatch) => {
        dispatch(newsRequest());
        axios.post('/news', news).then(() => {
            dispatch(newsRequest());
        }, error => {
            dispatch(newsFailure(error))
        })
    }
};

export const deleteNews = id => {
    return (dispatch) => {
        dispatch(newsRequest());
        axios.delete('/news/' + id).then(() => {
            dispatch(newsRequest());
        }, error => {
            dispatch(newsFailure(error))
        })
    }
};

export const oneNews = id => {
    return (dispatch) => {
        axios.get('/news/' + id).then(response => {
            dispatch(getOneNews(response.data));
        })
    }
};

export const getComment = id => {
    return (dispatch) => {
        dispatch(commentRequest());
        axios.get('/comment/' + id).then(response => {
            dispatch(commentSuccess(response.data));
        }, error => {
            dispatch(commentFailure(error))
        })
    }
};

export const postComment = comment => {
    return (dispatch) => {
        dispatch(commentRequest());
        axios.post('/comment', comment).then(() => {
            dispatch(commentRequest());
        }, error => {
            dispatch(commentFailure(error))
        })
    }
};

export const deleteComment = id => {
    return (dispatch) => {
        dispatch(commentRequest());
        axios.delete('/comment/' + id).then(() => {
            dispatch(commentRequest());
        }, error => {
            dispatch(commentFailure(error))
        })
    }
}