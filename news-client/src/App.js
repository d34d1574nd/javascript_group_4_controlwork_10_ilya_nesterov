import React, { Component } from 'react';

import './App.css';
import News from "./containers/News/News";
import {Route, Switch} from "react-router-dom";
import AddNews from "./containers/AddNews/AddNews";
import OneNews from "./containers/OneNews/OneNews";

class App extends Component {
  render() {
    return (
        <Switch>
            <Route path='/' exact component={News}/>
            <Route path='/news/:id' exact component={OneNews}/>
            <Route path='/addNews' exact component={AddNews}/>
            <Route render={() => <h1>Page not found</h1>}/>
        </Switch>
    );
  }
}

export default App;
