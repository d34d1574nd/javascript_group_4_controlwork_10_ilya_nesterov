import React, {Component} from 'react';
import {Nav, Navbar, NavbarBrand, NavItem, NavLink} from "reactstrap";

import './Navigation.css';

class Navigation extends Component {
    render() {
        return (
            <Navbar color="primary" dark className="navigation" expand="md">
                <NavbarBrand href="/">News</NavbarBrand>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink href="/">All news</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/addNews">Add news</NavLink>
                    </NavItem>
                </Nav>
            </Navbar>
        );
    }
}

export default Navigation;