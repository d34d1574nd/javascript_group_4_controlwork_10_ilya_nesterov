import React from 'react';
import {apiURL} from "../../constants";

import './NewsThimbnail.css';

const NewsThumbnail = props => {
    if (props.image !== null) {
        let image = apiURL + '/upload/' + props.image;
        return <img src={image} className="imageNews" alt="News" onError={image => image.target.style.display='none'}/>
    } return null
};

export default NewsThumbnail;