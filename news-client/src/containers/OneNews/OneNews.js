import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteComment, getComment, oneNews, postComment} from "../../store/actions";
import {
    Button,
    Card,
    CardBody,
    CardText,
    CardTitle,
    Col,
    Form,
    FormGroup,
    Input,
    Label,
    NavLink,
    Row
} from "reactstrap";
import NewsThumbnail from "../../components/NewsThumbnail/NewsThumbnail";

import './OneNews.css';


class OneNews extends Component {
    state = {
        author: '',
        comment: '',
    };

    componentDidMount () {
        this.props.oneNews(this.props.match.params.id);
        this.props.getComment(this.props.match.params.id);
    };

    inputChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitComment= event => {
        event.preventDefault();
        const comment = {
            idNews: this.props.match.params.id,
            author: this.state.author,
            comment: this.state.comment
        };
        if (this.state.author === '') {
            comment.author = 'Anonymous'
        }
        this.props.postComment(comment);
        this.props.history.push();
    };

    render() {
        return (
            <Fragment>
                <Card className="oneNews">
                    <NewsThumbnail image={this.props.getOneNews.image}/>
                    <CardBody>
                        <strong>Title:</strong>
                        <CardTitle>{this.props.getOneNews.titleNews}</CardTitle>
                        <strong>Description:</strong>
                        <CardText>{this.props.getOneNews.descriptionNews}</CardText>
                        <strong>Date publication news:</strong>
                        <p>{this.props.getOneNews.datePublicationNews}</p>
                        <Button outline color="primary"><NavLink href="/">Return to HomePage</NavLink></Button>
                    </CardBody>
                    {this.props.comments.map(item => {
                        return (
                            <Row key={item.id}>
                                <Col className="comment" sm="10">
                                    <Card body>
                                        <CardTitle><strong>Author: </strong>{item.author}</CardTitle>
                                        <CardText><strong>Comment: </strong>{item.comment}</CardText>
                                        <Col sm="6">
                                        <Button outline onClick={() => this.props.deleteComment(item.id)} color="danger">Delete</Button>
                                        </Col>
                                    </Card>
                                </Col>
                            </Row>
                        )
                    })}
                    <div className="formComment">
                        <Form onSubmit={this.submitComment}>
                            <FormGroup>
                                <Label for="author">Author</Label>
                                <Input
                                    type="title" name="author"
                                    id="author"
                                    placeholder="Anonymous"
                                    value={this.state.author}
                                    onChange={this.inputChange}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label for="comment">Comment</Label>
                                <Input
                                    type="title" name="comment"
                                    id="comment"
                                    placeholder="Enter comment to this news"
                                    value={this.state.comment}
                                    onChange={this.inputChange}
                                />
                            </FormGroup>
                            <FormGroup check row>
                                <Col sm={{ size: 10, offset: 2 }}>
                                    <Button outline color="primary" type="submit">Submit</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </div>
                </Card>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    getOneNews: state.getOneNews,
    comments: state.comments
});

const mapDispatchToProps = dispatch => ({
    oneNews: id => dispatch(oneNews(id)),
    getComment: id => dispatch(getComment(id)),
    postComment: comment => dispatch(postComment(comment)),
    deleteComment: id => dispatch(deleteComment(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OneNews);