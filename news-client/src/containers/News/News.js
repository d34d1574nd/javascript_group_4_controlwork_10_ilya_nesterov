import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteNews, gettingNews} from "../../store/actions";
import {Button, ButtonGroup, Card, CardTitle, Col, NavLink, Row} from "reactstrap";
import NewsThumbnail from "../../components/NewsThumbnail/NewsThumbnail";

import './News.css';


class News extends Component {

    componentDidMount () {
        this.props.getNews();
    };

    render() {
        return (
            <div className="newsList">
                <Row>
                    {this.props.news.map(item => {
                        return (
                            <Col sm="6" key={item.id}>
                                <Card body className="news">
                                    <p className='date'><span>date publication: </span>{item.datePublicationNews}</p>
                                    <NewsThumbnail image={item.image}/>
                                    <CardTitle className='title'><span>News title: </span>{item.titleNews}</CardTitle>
                                    <ButtonGroup>
                                        <Button outline color="primary"><NavLink href={"/news/" + item.id}>Go to news description</NavLink></Button>
                                        <Button outline onClick={() => this.props.deleteNews(item.id)} color="danger">Delete</Button>
                                    </ButtonGroup>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    news: state.news,
});

const mapDispatchToProps = dispatch => ({
    getNews: () => dispatch(gettingNews()),
    deleteNews: id => dispatch(deleteNews(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(News);