import React, {Component} from 'react';

import {connect} from "react-redux";
import {addNewNews} from "../../store/actions";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

import './AddNews.css';

class AddNews extends Component {
    state = {
        title: '',
        description: '',
        image: null,
    };

    submitNews = event => {
        event.preventDefault();
        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.addNews(formData);
    };

    inputChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChange = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <div className="form-news">
                <Form onSubmit={this.submitNews}>
                    <FormGroup>
                        <Label for="title">Title news</Label>
                        <Input
                            type="title" name="title"
                            id="title"
                            placeholder="Enter news title"
                            value={this.state.title}
                            onChange={this.inputChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="description">News description</Label>
                        <Input
                            type="textarea" name="description"
                            id="description"
                            placeholder="Enter news description"
                            value={this.state.description}
                            onChange={this.inputChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="image">Image for news</Label>
                        <Input
                            type="file" name="image"
                            id="image"
                            onChange={this.fileChange}
                        />
                    </FormGroup>
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 2 }}>
                            <Button outline color="primary" type="submit">Submit</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addNews: news => dispatch(addNewNews(news))
});

export default connect(null, mapDispatchToProps)(AddNews);