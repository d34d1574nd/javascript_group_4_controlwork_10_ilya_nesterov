CREATE SCHEMA `news` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE `news`.`news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titleNews` VARCHAR(255) NOT NULL,
  `descriptionNews` TEXT NOT NULL,
  `image` VARCHAR(100) NULL,
  `datePublicationNews` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `news`.`comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idNews` INT NULL,
  `author` VARCHAR(255) NULL,
  `comment` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `news_fk_idx` (`idNews` ASC),
  CONSTRAINT `news_fk`
    FOREIGN KEY (`idNews`)
    REFERENCES `news`.`news` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);