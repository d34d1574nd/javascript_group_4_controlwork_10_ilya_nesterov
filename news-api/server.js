const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const news = require('./app/news/news');
const comment = require('./app/comment/comment');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

const connection = mysql.createConnection( {
    host: "localhost",
    user: "d34d1574nd",
    password: "F34rD34n",
    database: "news"
});

app.use('/news', news(connection));
app.use('/comment', comment(connection));

connection.connect(err => {
    if (err) {
        console.error("error" + err.stack);
        return;
    }
    console.log("Connection to sql" + connection.threadId);

    app.listen(port, () => {
        console.log("Our port " + port)
    })
});