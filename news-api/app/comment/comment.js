const express = require('express');

const createRouter = connection => {
    const router = express.Router();

    router.get('/:id', (req, res) => {
        connection.query('SELECT `comments`.`id`, `author`, `comment` FROM `comments` INNER JOIN `news` ON `comments`.`idNews` = `news`.`id` WHERE `news`.`id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send(results);
        })
    });

    router.post('/', (req, res) => {
        const item = req.body;

        connection.query('INSERT INTO `comments` (`idNews`, `author`, `comment`) VALUES (?, ?, ?)',
            [item.idNews, item.author, item.comment],
            (error, results) => {
                if (error) {
                    res.status(500).send({error: 'Database error'});
                }
                res.send({message: 'OK'});
            });
    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `comments` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send({message: 'Delete ok'});
        });
    });

    return router
};

module.exports = createRouter;